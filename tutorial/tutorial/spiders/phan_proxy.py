import logging
from selenium import webdriver

import logging
from  random  import choice
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import WebDriverException
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_REQ = os.path.join(BASE_DIR, 'avv_work_taboola_3')
FNAME = os.path.join(DIR_REQ, 'proxies.txt')

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )


def main(link):
    f2 = open(FNAME)
    proxy_list = f2.read().strip().split("\n")
    f2.close()

    for l in xrange(3):
        ip_port = choice(proxy_list).strip()
        logging.debug(ip_port)

        user_pass = ":".join(ip_port.split(":")[-2:]).strip()
        prox = "--proxy=%s" % ":".join((ip_port.split(":")[:2])).strip()

        service_args = [prox, '--proxy-auth='+user_pass, '--proxy-type=http', '--load-images=no']
        # service_args = [prox, '--proxy-type=http', '--load-images=no']

        logging.debug(service_args)

        dcap = dict(DesiredCapabilities.PHANTOMJS)

        dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:23.0) Gecko/20100101 Firefox/23.0")
        dcap["--disable-popup-blocking"] = False

        driver = webdriver.PhantomJS(service_args = service_args, desired_capabilities=dcap)
        #driver.start_session(desired_capabilities=dcap)
        #driver.get(link)
        #driver.refresh()
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.set_page_load_timeout(15)

        try:
            driver.get(link)
        except:
            pass

        if str(driver.current_url).strip() != "about:blank":
            return driver

        else:
            driver.delete_all_cookies()
            driver.quit()

    return None


if __name__=="__main__":
    link = "http://www.jabong.com/"
    driver = main(link)
    page = driver.page_source
    print page

